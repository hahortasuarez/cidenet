-- insert table countries
insert into countries (id, country)
values (1, 'Colombia');
insert into countries (id, country)
values (2, 'Estados Unidos');

-- insert table areas
    insert
into areas (id, area)
values (1, 'Administración');
insert into areas (id, area)
values (2, 'Financiera');
insert into areas (id, area)
values (3, 'Compras');
insert into areas (id, area)
values (4, 'Infraestructura');
insert into areas (id, area)
values (5, 'Operación');
insert into areas (id, area)
values (6, 'Talento Humano');
insert into areas (id, area)
values (7, 'Servicios Varios');
insert into areas (id, area)
values (8, 'Desarrollo software');

-- insert table type_document
insert
into type_document (id, type_document)
values (1, 'Cédula de Ciudadanía');
insert into type_document (id, type_document)
values (2, 'Cédula de Extranjería');
insert into type_document (id, type_document)
values (3, 'Pasaporte');
insert into type_document (id, type_document)
values (4, 'Permiso Especial');