package com.cidenet.web.advisers;

import com.cidenet.web.advisers.base.BaseRestAdviser;
import com.cidenet.web.api.AreasApi;
import com.cidenet.web.api.CountriesApi;
import com.cidenet.web.api.EmployeesApi;
import com.cidenet.web.api.TypeDocumentApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * The type Person rest advicer.
 */
@Slf4j
@ControllerAdvice(assignableTypes = {

        EmployeesApi.class,
        CountriesApi.class,
        AreasApi.class,
        TypeDocumentApi.class,


})
public class GeneralRestAdvice extends BaseRestAdviser {

}
