package com.cidenet.web.api;

import com.cidenet.model.payload.response.countries.ResponseCountries;
import com.cidenet.services.countries.CountriesService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * The type Countries api.
 */
@RestController
@RequestMapping(value = "/api/countries")
@CrossOrigin({"*"})
public class CountriesApi {

    private final CountriesService countriesService;

    /**
     * Instantiates a new Countries api.
     *
     * @param countriesService the countries service
     */
    public CountriesApi(CountriesService countriesService) {
        this.countriesService = countriesService;
    }

    /**
     * Gets all countries.
     *
     * @return the all countries
     */
    @GetMapping()
    public List<ResponseCountries> getAllCountries() {
        return countriesService.getAllCountries();
    }
}
