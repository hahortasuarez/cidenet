package com.cidenet.web.api;

import com.cidenet.model.payload.request.RequestAddEmployees;
import com.cidenet.model.payload.response.employees.ResponseEmployees;
import com.cidenet.services.employees.EmployeesService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

/**
 * The type Employees api.
 */
@RestController
@RequestMapping(value = "/api/employees")
@CrossOrigin({"*"})
public class EmployeesApi {

    private final EmployeesService employeesService;

    /**
     * Instantiates a new Employees api.
     *
     * @param employeesService the employees service
     */
    public EmployeesApi(EmployeesService employeesService) {
        this.employeesService = employeesService;
    }

    /**
     * Gets all employees.
     *
     * @return the all employees
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ResponseEmployees> getAllEmployees() {
        return employeesService.getAll();
    }

    /**
     * Gets by id.
     *
     * @param id the id
     * @return the by id
     */
    @GetMapping("/{id}")
    public ResponseEntity<ResponseEmployees> getById(@PathVariable("id") Integer id) {
        ResponseEmployees employees = employeesService.getById(id);
        return new ResponseEntity<>(employees, new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * Create response entity.
     *
     * @param requestAddEmployees the request add employees
     * @return the response entity
     * @throws ParseException the parse exception
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseEmployees> create(@RequestBody RequestAddEmployees requestAddEmployees) throws ParseException {
        ResponseEmployees created = employeesService.create(requestAddEmployees);
        return new ResponseEntity<>(created, new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * Update response entity.
     *
     * @param id                  the id
     * @param requestAddEmployees the request add employees
     * @return the response entity
     * @throws ParseException the parse exception
     */
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseEmployees> update(@PathVariable Integer id, @RequestBody RequestAddEmployees requestAddEmployees) throws ParseException {
        ResponseEmployees update = employeesService.update(id,requestAddEmployees);
        return new ResponseEntity<>(update, new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * Delete.
     *
     * @param id the id
     */
    @DeleteMapping(value = "/{id}")
    public void delete(@PathVariable Integer id) {
        employeesService.deleteById(id);
    }
}
