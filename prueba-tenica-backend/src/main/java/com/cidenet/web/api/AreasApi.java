package com.cidenet.web.api;

import com.cidenet.model.payload.response.areas.ResponseAreas;
import com.cidenet.services.areas.AreasService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * The type Areas api.
 */
@RestController
@RequestMapping(value = "/api/areas")
@CrossOrigin({"*"})
public class AreasApi {

    private final AreasService areasService;

    /**
     * Instantiates a new Areas api.
     *
     * @param areasService the areas service
     */
    public AreasApi(AreasService areasService) {
        this.areasService = areasService;
    }

    /**
     * Gets all countries.
     *
     * @return the all countries
     */
    @GetMapping()
    public List<ResponseAreas> getAllCountries() {
        return areasService.getAllAreas();
    }
}
