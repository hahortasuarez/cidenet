package com.cidenet.web.api;

import com.cidenet.model.payload.response.typedocument.ResponseTypeDocument;
import com.cidenet.services.typedocument.TypeDocumentService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * The type Type document api.
 */
@RestController
@RequestMapping(value = "/api/typeDocument")
@CrossOrigin({"*"})
public class TypeDocumentApi {

    private final TypeDocumentService typeDocumentService;

    /**
     * Instantiates a new Type document api.
     *
     * @param typeDocumentService the type document service
     */
    public TypeDocumentApi(TypeDocumentService typeDocumentService) {
        this.typeDocumentService = typeDocumentService;
    }

    /**
     * Gets all type documents.
     *
     * @return the all type documents
     */
    @GetMapping()
    public List<ResponseTypeDocument> getAllTypeDocuments() {
        return typeDocumentService.getAllTypeDocument();
    }
}
