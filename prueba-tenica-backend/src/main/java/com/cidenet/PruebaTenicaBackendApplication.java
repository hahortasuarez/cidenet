package com.cidenet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaTenicaBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(PruebaTenicaBackendApplication.class, args);
    }

}
