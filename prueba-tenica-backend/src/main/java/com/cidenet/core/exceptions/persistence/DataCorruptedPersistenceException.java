package com.cidenet.core.exceptions.persistence;


import com.cidenet.core.exceptions.base.ServiceException;
import com.cidenet.core.exceptions.enums.LogRefServices;

/**
 * The type Data corrupted persistence exception.
 */
public class DataCorruptedPersistenceException extends ServiceException {

    /**
     * Instantiates a new Data corrupted persistence exception.
     *
     * @param logRefServices the log ref services
     * @param message        the message
     */
    public DataCorruptedPersistenceException(LogRefServices logRefServices, String message){
        super(logRefServices, message);
    }

    /**
     * Instantiates a new Data corrupted persistence exception.
     *
     * @param logRefServices the log ref services
     * @param message        the message
     * @param cause          the cause
     */
    public DataCorruptedPersistenceException(LogRefServices logRefServices, String message, Throwable cause){
        super(logRefServices, message,cause);
    }
}
