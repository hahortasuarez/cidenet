package com.cidenet.core.exceptions.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * The enum Log ref servicios.
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum LogRefServices {

    /**
     * Error general servicio log ref servicios.
     */
    LOG_REF_SERVICES("LOG_REF_SERVICES", "/ayuda/error_general_servicio.html"),
    /**
     * Error persistencia log ref servicios.
     */
    ERROR_PERSISTENCE("ERROR_PERSISTENCE", "/ayuda/error_persistencia.html"),
    /**
     * Error dato corrupto log ref servicios.
     */
    ERROR_DATA_CORRUPT("ERROR_DATA_CORRUPT", "/ayuda/error_dato_corrupto.html"),

    ERROR_DATA_NOT_FOUND("ERROR_DATA_NOT_FOUND", "/ayuda/error_general_servicio.html"),
      /**
     * Error llave parametro no encontrado log ref servicios.
     */
    ERROR_SAVE("ERROR_SAVE", "/ayuda/error_general/error_guardar_solicitud.html");

    /**
     * Codigo del error
     */
    @Getter
    public final String logRef;
    /**
     * Enlace a la pagina con ayuda
     */
    @Getter
    final
    String hrefLink;

}

