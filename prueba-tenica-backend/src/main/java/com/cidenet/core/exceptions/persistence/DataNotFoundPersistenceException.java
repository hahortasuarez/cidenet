package com.cidenet.core.exceptions.persistence;

import com.cidenet.core.exceptions.base.ServiceException;
import com.cidenet.core.exceptions.enums.LogRefServices;

/**
 * The type Data not found persistence exception.
 */
public class DataNotFoundPersistenceException extends ServiceException {

    /**
     * Instantiates a new Data not found persistence exception.
     *
     * @param logRefServices the log ref services
     * @param message        the message
     */
    public DataNotFoundPersistenceException(LogRefServices logRefServices, String message){
        super(logRefServices, message);
    }

    /**
     * Instantiates a new Data not found persistence exception.
     *
     * @param logRefServices the log ref services
     * @param message        the message
     * @param cause          the cause
     */
    public DataNotFoundPersistenceException(LogRefServices logRefServices, String message, Throwable cause){
        super(logRefServices, message, cause);
    }
}
