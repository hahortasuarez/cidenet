package com.cidenet.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * The type Format date util.
 */
public class FormatDateUtil {

    private FormatDateUtil() {
        throw new IllegalStateException("paginationUtil class");
    }

    /**
     * Format date date.
     *
     * @return the date
     * @throws ParseException the parse exception
     */
    public static Date formatDate() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        String dateInString = sdf.format(new Date());
        return sdf.parse(dateInString);
    }

    /**
     * Convert date string.
     *
     * @param date the date
     * @return the string
     */
    public static String convertDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
        return sdf.format(date);
    }

    /**
     * Validate mount before date.
     *
     * @return the date
     */
    public static Date validateMountBefore() {
        int dias = 31;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_YEAR, -dias);
        return calendar.getTime();

    }
}
