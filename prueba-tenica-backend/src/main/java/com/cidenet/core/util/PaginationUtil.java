package com.cidenet.core.util;

import org.springframework.data.domain.Pageable;

public class PaginationUtil {

    private PaginationUtil() { throw new IllegalStateException("paginationUtil class");}

    public static int getLimitPaginator(Pageable pageable, int limitMin, int limitMax) {
        int limit = limitMin;
        if (pageable.getPageNumber() != 0) {
            limit = limitMax;
        }
        return limit;
    }
}
