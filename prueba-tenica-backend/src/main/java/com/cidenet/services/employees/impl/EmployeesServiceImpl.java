package com.cidenet.services.employees.impl;

import com.cidenet.core.exceptions.enums.LogRefServices;
import com.cidenet.core.exceptions.persistence.DataCorruptedPersistenceException;
import com.cidenet.core.util.FormatDateUtil;
import com.cidenet.model.entity.Areas;
import com.cidenet.model.entity.Countries;
import com.cidenet.model.entity.Employees;
import com.cidenet.model.entity.TypeDocuments;
import com.cidenet.model.enums.Status;
import com.cidenet.model.payload.request.RequestAddEmployees;
import com.cidenet.model.payload.response.employees.ResponseEmployees;
import com.cidenet.repositories.areas.facade.AreasRepositoryFacade;
import com.cidenet.repositories.countries.facade.CountriesRepositoryFacade;
import com.cidenet.repositories.employees.facade.EmployeesRepositoryFacade;
import com.cidenet.repositories.typedocument.facade.TypeDocumentRepositoryFacade;
import com.cidenet.services.employees.EmployeesService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;

/**
 * The type Employees service.
 */
@Service
public class EmployeesServiceImpl implements EmployeesService {

    private final EmployeesRepositoryFacade employeesRepositoryFacade;

    private final CountriesRepositoryFacade countriesRepositoryFacade;

    private final AreasRepositoryFacade areasRepositoryFacade;

    private final TypeDocumentRepositoryFacade typeDocumentRepositoryFacade;


    @Value("${spring.domain.colombia}")
    private String domainColombia;


    @Value("${spring.domain.us}")
    private String domainUS;


    /**
     * The Pattern.
     */
    Pattern pattern = Pattern
            .compile("^([a-zA-Z]+(\\.*[a-zA-Z]+)*[a-zA-Z])$");

    /**
     * Instantiates a new Employees service.
     *
     * @param employeesRepositoryFacade    the employees repository facade
     * @param countriesRepositoryFacade    the countries repository facade
     * @param areasRepositoryFacade        the areas repository facade
     * @param typeDocumentRepositoryFacade the type document repository facade
     */
    public EmployeesServiceImpl(EmployeesRepositoryFacade employeesRepositoryFacade, CountriesRepositoryFacade countriesRepositoryFacade, AreasRepositoryFacade areasRepositoryFacade, TypeDocumentRepositoryFacade typeDocumentRepositoryFacade) {
        this.employeesRepositoryFacade = employeesRepositoryFacade;
        this.countriesRepositoryFacade = countriesRepositoryFacade;
        this.areasRepositoryFacade = areasRepositoryFacade;
        this.typeDocumentRepositoryFacade = typeDocumentRepositoryFacade;
    }

    @Override
    public List<ResponseEmployees> getAll() {
        return employeesRepositoryFacade.getAllEmployees().stream().map(this::dtoMapperEmployeesEntity).collect(Collectors.toList());
    }

    @Override
    public ResponseEmployees getById(Integer id) {
        return dtoMapperEmployeesEntity(employeesRepositoryFacade.getById(id));
    }

    @Override
    public ResponseEmployees create(RequestAddEmployees requestAddEmployees) throws ParseException {

        Countries country = getCountries(requestAddEmployees.getCountryId());
        Areas area = getAreas(requestAddEmployees.getAreaId());
        TypeDocuments typeDocuments = getTypeDocument(requestAddEmployees.getTypeDocumentId());

        String surname = requestAddEmployees.getSurname().replace(" ", "").toLowerCase();
        String email = generateEmail(requestAddEmployees.getFirstName().toLowerCase(), surname, country);

        validation(requestAddEmployees, email);

        Employees employees = entityMapperEmployeesDTO(requestAddEmployees, country, area, typeDocuments, email);

        return dtoMapperEmployeesEntity(employeesRepositoryFacade.create(employees));

    }

    @Override
    public ResponseEmployees update(Integer id, RequestAddEmployees requestAddEmployees) throws ParseException {

        Employees employee = employeesRepositoryFacade.getById(id);
        Countries countryNew = getCountries(requestAddEmployees.getCountryId());
        Countries country = getCountries(employee.getCountryId().getId());
        TypeDocuments typeDocumentsNew = getTypeDocument(requestAddEmployees.getTypeDocumentId());
        TypeDocuments typeDocuments = getTypeDocument(employee.getTypeDocumentsId().getId());

        String surname = requestAddEmployees.getSurname().replace(" ", "").toLowerCase();
        String email = employee.getEmail();

        if (!country.getCountry().equals(countryNew.getCountry()) || !Objects.equals(requestAddEmployees.getFirstName().toUpperCase(), employee.getFirstName()) || !Objects.equals(requestAddEmployees.getSurname().toUpperCase(), employee.getSurname())) {
            email = generateEmail(requestAddEmployees.getFirstName().toLowerCase(), surname, countryNew);
        }
        Areas areaNew = getAreas(requestAddEmployees.getAreaId());
        Areas area = getAreas(employee.getAreaId().getId());

        employee.setSurname(defaultIfNull(requestAddEmployees.getSurname().trim(), employee.getSurname()));
        employee.setSecondSurname(defaultIfNull(requestAddEmployees.getSecondSurname().trim(), employee.getSecondSurname()));
        employee.setFirstName(defaultIfNull(requestAddEmployees.getFirstName().trim(), employee.getFirstName()));
        employee.setOtherNames(defaultIfNull(requestAddEmployees.getOtherNames(), employee.getOtherNames()));
        employee.setCountryId(defaultIfNull(countryNew, country));
        employee.setTypeDocumentsId(defaultIfNull(typeDocumentsNew, typeDocuments));
        employee.setIdentificationNumber(defaultIfNull(requestAddEmployees.getIdentificationNumber(), employee.getIdentificationNumber()));
        employee.setEmail(email);
        employee.setAdmissionDate(defaultIfNull(requestAddEmployees.getAdmissionDate(), employee.getAdmissionDate()));
        employee.setAreaId(defaultIfNull(areaNew, area));
        employee.setStatus(employee.getStatus());
        employee.setRegistrationDate(employee.getRegistrationDate());
        employee.setUpdateDate(FormatDateUtil.formatDate());

        return dtoMapperEmployeesEntity(employeesRepositoryFacade.create(employee));

    }

    private void validation(RequestAddEmployees requestAddEmployees, String email) {

        String surname = requestAddEmployees.getSurname().replace(" ", "");

        if (employeesRepositoryFacade.existsByIdentificationNumber(requestAddEmployees.getIdentificationNumber())) {
            throw new DataCorruptedPersistenceException(LogRefServices.ERROR_DATA_CORRUPT, "el numero de identificación " + requestAddEmployees.getIdentificationNumber() + " del empleado ya existe");
        }
        Matcher surnameValid = pattern.matcher(surname);
        Matcher secondSurnameValid = pattern.matcher(requestAddEmployees.getSecondSurname().trim());
        Matcher firstNameValid = pattern.matcher(requestAddEmployees.getFirstName().trim());
        Matcher otherNameValid = pattern.matcher(requestAddEmployees.getOtherNames().trim());

        if (!surnameValid.find() && requestAddEmployees.getSecondSurname().length() < 20) {
            throw new DataCorruptedPersistenceException(LogRefServices.ERROR_DATA_CORRUPT, "el primer apellido no debe contener acentos, ñ, ni contener mas de 20 caracteres");
        }

        if (!secondSurnameValid.find() && requestAddEmployees.getSecondSurname().length() < 20) {
            throw new DataCorruptedPersistenceException(LogRefServices.ERROR_DATA_CORRUPT, "el segundo apellido no debe contener acentos, ñ, ni contener mas de 20 caracteres");
        }

        if (!firstNameValid.find() && requestAddEmployees.getFirstName().length() < 20) {
            throw new DataCorruptedPersistenceException(LogRefServices.ERROR_DATA_CORRUPT, "el primer nombre no debe contener acentos , ñ, ni contener mas de 20 caracteres");
        }

        if (!otherNameValid.find() && requestAddEmployees.getSecondSurname().length() < 50) {
            throw new DataCorruptedPersistenceException(LogRefServices.ERROR_DATA_CORRUPT, "los otros nombres no debe contener acentos, ñ, ni contener mas de 50 caracteres");
        }

        if (requestAddEmployees.getAdmissionDate().after(new Date())) {
            throw new DataCorruptedPersistenceException(LogRefServices.ERROR_DATA_CORRUPT, "La fecha de admisión no puede ser superior a la fecha actual");
        }

        if (requestAddEmployees.getAdmissionDate().before(FormatDateUtil.validateMountBefore())) {
            throw new DataCorruptedPersistenceException(LogRefServices.ERROR_DATA_CORRUPT, "La fecha de admisión     no puede tener mas de 30 dias de anterioridad");
        }
        if (email.length() > 300) {
            throw new DataCorruptedPersistenceException(LogRefServices.ERROR_DATA_CORRUPT, "el email no puede tener mas de 300 caracteres");
        }
    }

    @Override
    public void deleteById(Integer id) {

        if (!employeesRepositoryFacade.existsById(id)) {
            throw new DataCorruptedPersistenceException(LogRefServices.ERROR_DATA_CORRUPT, "NO se encontraron registros");
        }
        employeesRepositoryFacade.deleteById(id);
    }

    private String generateEmail(String firstName, String surname, Countries country) {

        StringBuilder concatName = null;
        StringBuilder email = getValidateCountryEmail(firstName, surname, country);

        int cont = 0;
        boolean existEmail = employeesRepositoryFacade.existsByEmail(email.toString());

        if (!existEmail) {
            concatName = getValidateCountryEmail(firstName, surname, country);
        }
        while (existEmail) {
            concatName = new StringBuilder(firstName + "." + surname);
            cont++;
            String numCadena = String.valueOf(cont);
            if (country.getId() != 2) {
                concatName.append(".").append(numCadena).append(domainColombia);
            } else {
                concatName.append(".").append(numCadena).append(domainUS);
            }
            if (!employeesRepositoryFacade.existsByEmail(concatName.toString())
            ) {
                existEmail = false;
            } else {
                concatName.setLength(0);
            }
        }
        return concatName.toString();
    }

    private StringBuilder getValidateCountryEmail(String firstName, String surname, Countries country) {
        StringBuilder email = new StringBuilder(firstName + "." + surname);
        if (country.getCountry().equals("Colombia")) {
            email = new StringBuilder(email + domainColombia);
        } else {
            email = new StringBuilder(email + domainUS);
        }
        return email;
    }

    private Areas getAreas(Integer idArea) {
        Areas area = areasRepositoryFacade.getAreaById(idArea);
        Areas areas = new Areas();
        areas.setId(area.getId());
        areas.setArea(area.getArea());
        return areas;
    }

    private TypeDocuments getTypeDocument(Integer id) {
        TypeDocuments typeDocuments = typeDocumentRepositoryFacade.getById(id);
        TypeDocuments typeDocument = new TypeDocuments();
        typeDocument.setId(typeDocuments.getId());
        typeDocument.setTypeDocument(typeDocuments.getTypeDocument());
        return typeDocument;
    }

    private Countries getCountries(Integer idCountries) {
        Countries country = countriesRepositoryFacade.getCountriesById(idCountries);
        Countries countries = new Countries();
        countries.setId(country.getId());
        countries.setCountry(country.getCountry());
        return countries;
    }

    private Employees entityMapperEmployeesDTO(RequestAddEmployees requestAddEmployees, Countries country, Areas area, TypeDocuments typeDocuments, String email) throws ParseException {

        Employees employees = new Employees();

        employees.setSurname(requestAddEmployees.getSurname().trim().toUpperCase());
        employees.setSecondSurname(requestAddEmployees.getSecondSurname().trim().toUpperCase());
        employees.setFirstName(requestAddEmployees.getFirstName().trim().toUpperCase());
        employees.setOtherNames(requestAddEmployees.getOtherNames().toUpperCase());
        employees.setCountryId(country);
        employees.setTypeDocumentsId(typeDocuments);
        employees.setIdentificationNumber(requestAddEmployees.getIdentificationNumber());
        employees.setEmail(email);
        employees.setAdmissionDate(requestAddEmployees.getAdmissionDate());
        employees.setAreaId(area);
        employees.setStatus(Status.ACTIVO);
        employees.setRegistrationDate(FormatDateUtil.formatDate());
        employees.setUpdateDate(FormatDateUtil.formatDate());
        return employees;
    }

    private ResponseEmployees dtoMapperEmployeesEntity(Employees employees) {

        Countries country = getCountries(employees.getCountryId().getId());
        Areas area = getAreas(employees.getAreaId().getId());
        TypeDocuments typeDocuments = getTypeDocument(employees.getTypeDocumentsId().getId());
        ResponseEmployees responseEmployees = new ResponseEmployees();

        responseEmployees.setId(employees.getId());
        responseEmployees.setSurname(employees.getSurname());
        responseEmployees.setSecondSurname(employees.getSecondSurname());
        responseEmployees.setFirstName(employees.getFirstName());
        responseEmployees.setOtherNames(employees.getOtherNames());
        responseEmployees.setCountryId(country);
        responseEmployees.setTypeDocumentsId(typeDocuments);
        responseEmployees.setIdentificationNumber(employees.getIdentificationNumber());
        responseEmployees.setEmail(employees.getEmail());
        responseEmployees.setAdmissionDate(FormatDateUtil.convertDate(employees.getAdmissionDate()));
        responseEmployees.setArea(area);
        responseEmployees.setStatus(employees.getStatus());
        responseEmployees.setRegistrationDate(FormatDateUtil.convertDate(employees.getRegistrationDate()));
        responseEmployees.setUpdateDate(FormatDateUtil.convertDate(employees.getUpdateDate()));
        return responseEmployees;
    }
}
