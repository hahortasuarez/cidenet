package com.cidenet.services.employees;

import com.cidenet.model.payload.request.RequestAddEmployees;
import com.cidenet.model.payload.response.employees.ResponseEmployees;

import java.text.ParseException;
import java.util.List;

/**
 * The interface Employees service.
 */
public interface EmployeesService {
    /**
     * Gets all.
     *
     * @return the all
     */
    List<ResponseEmployees> getAll();

    /**
     * Gets by id.
     *
     * @param id the id
     * @return the by id
     */
    ResponseEmployees getById(Integer id);

    /**
     * Create response employees.
     *
     * @param requestAddEmployees the request add employees
     * @return the response employees
     * @throws ParseException the parse exception
     */
    ResponseEmployees create(RequestAddEmployees requestAddEmployees) throws ParseException;

    /**
     * Update response employees.
     *
     * @param id                  the id
     * @param requestAddEmployees the request add employees
     * @return the response employees
     * @throws ParseException the parse exception
     */
    ResponseEmployees update(Integer id, RequestAddEmployees requestAddEmployees) throws ParseException;

    /**
     * Delete by id.
     *
     * @param id the id
     */
    void deleteById(Integer id);
}
