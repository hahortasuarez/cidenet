package com.cidenet.services.areas;

import com.cidenet.model.payload.response.areas.ResponseAreas;

import java.util.List;

/**
 * The interface Areas service.
 */
public interface AreasService {
    /**
     * Gets all areas.
     *
     * @return the all areas
     */
    List<ResponseAreas> getAllAreas();
}
