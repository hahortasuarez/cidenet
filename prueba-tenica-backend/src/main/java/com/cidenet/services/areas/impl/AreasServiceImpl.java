package com.cidenet.services.areas.impl;

import com.cidenet.model.entity.Areas;
import com.cidenet.model.payload.response.areas.ResponseAreas;
import com.cidenet.repositories.areas.facade.AreasRepositoryFacade;
import com.cidenet.services.areas.AreasService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Areas service.
 */
@Service
public class AreasServiceImpl implements AreasService {

    private final AreasRepositoryFacade areasRepositoryFacade;

    /**
     * Instantiates a new Areas service.
     *
     * @param areasRepositoryFacade the areas repository facade
     */
    public AreasServiceImpl(AreasRepositoryFacade areasRepositoryFacade) {
        this.areasRepositoryFacade = areasRepositoryFacade;
    }

    @Override
    public List<ResponseAreas> getAllAreas() {
        List<Areas> areas = areasRepositoryFacade.getAllAreas();
        return areas.stream().map(this::mapperAreasEntity).collect(Collectors.toList());
    }

    private ResponseAreas mapperAreasEntity (Areas areas){
        return ResponseAreas.builder().id(areas.getId()).area(areas.getArea()).build();
    }
}
