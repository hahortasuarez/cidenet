package com.cidenet.services.countries.impl;

import com.cidenet.model.entity.Countries;
import com.cidenet.model.payload.response.countries.ResponseCountries;
import com.cidenet.repositories.countries.facade.CountriesRepositoryFacade;
import com.cidenet.services.countries.CountriesService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Countries service.
 */
@Service
public class CountriesServiceImpl implements CountriesService {

    private final CountriesRepositoryFacade countriesRepositoryFacade;

    /**
     * Instantiates a new Countries service.
     *
     * @param countriesRepositoryFacade the countries repository facade
     */
    public CountriesServiceImpl(CountriesRepositoryFacade countriesRepositoryFacade) {
        this.countriesRepositoryFacade = countriesRepositoryFacade;
    }

    @Override
    public List<ResponseCountries> getAllCountries() {
        List<Countries> countries = countriesRepositoryFacade.getAllCountries();
        return countries.stream().map(this::mapperCountriesEntity).collect(Collectors.toList());
    }

    private ResponseCountries mapperCountriesEntity(Countries countries) {
        return ResponseCountries.builder().id(countries.getId()).country(countries.getCountry()).build();
    }
}
