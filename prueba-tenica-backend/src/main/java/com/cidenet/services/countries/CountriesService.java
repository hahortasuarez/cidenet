package com.cidenet.services.countries;

import com.cidenet.model.payload.response.countries.ResponseCountries;

import java.util.List;

/**
 * The interface Countries service.
 */
public interface CountriesService {
    /**
     * Gets all countries.
     *
     * @return the all countries
     */
    List<ResponseCountries> getAllCountries();
}
