package com.cidenet.services.typedocument.impl;

import com.cidenet.model.entity.TypeDocuments;
import com.cidenet.model.payload.response.typedocument.ResponseTypeDocument;
import com.cidenet.repositories.typedocument.facade.TypeDocumentRepositoryFacade;
import com.cidenet.services.typedocument.TypeDocumentService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * The type Type document service.
 */
@Service
public class TypeDocumentServiceImpl implements TypeDocumentService {

    private final TypeDocumentRepositoryFacade typeDocumentRepositoryFacade;

    /**
     * Instantiates a new Type document service.
     *
     * @param typeDocumentRepositoryFacade the type document repository facade
     */
    public TypeDocumentServiceImpl(TypeDocumentRepositoryFacade typeDocumentRepositoryFacade) {
        this.typeDocumentRepositoryFacade = typeDocumentRepositoryFacade;
    }

    @Override
    public List<ResponseTypeDocument> getAllTypeDocument() {
        List<TypeDocuments> typeDocuments = typeDocumentRepositoryFacade.getAllTypeDocument();
        return typeDocuments.stream().map(this::mapperTypeDocumentEntity).collect(Collectors.toList());
    }
    private ResponseTypeDocument mapperTypeDocumentEntity (TypeDocuments typeDocuments){
        return ResponseTypeDocument.builder().id(typeDocuments.getId()).typeDocument(typeDocuments.getTypeDocument()).build();
    }
}
