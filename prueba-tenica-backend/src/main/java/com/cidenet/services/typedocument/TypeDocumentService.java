package com.cidenet.services.typedocument;

import com.cidenet.model.payload.response.typedocument.ResponseTypeDocument;

import java.util.List;

/**
 * The interface Type document service.
 */
public interface TypeDocumentService {
    /**
     * Gets all type document.
     *
     * @return the all type document
     */
    List<ResponseTypeDocument> getAllTypeDocument();
}
