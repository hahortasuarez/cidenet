package com.cidenet.repositories.countries.facade.impl;

import com.cidenet.core.constants.MessageError;
import com.cidenet.core.exceptions.enums.LogRefServices;
import com.cidenet.core.exceptions.persistence.DataNotFoundPersistenceException;
import com.cidenet.model.entity.Countries;
import com.cidenet.repositories.countries.CountriesRepository;
import com.cidenet.repositories.countries.facade.CountriesRepositoryFacade;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The type Countries repository facade.
 */
@Component
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class CountriesRepositoryFacadeImpl implements CountriesRepositoryFacade {

    private final CountriesRepository countriesRepository;

    /**
     * Instantiates a new Countries repository facade.
     *
     * @param countriesRepository the countries repository
     */
    public CountriesRepositoryFacadeImpl(CountriesRepository countriesRepository) {
        this.countriesRepository = countriesRepository;
    }

    @Override
    public List<Countries> getAllCountries() {
        try {
            return Optional.of(countriesRepository.findAll())
                    .orElseThrow(() -> new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, "No se encontraron registros de paises"));
        } catch (EmptyResultDataAccessException er) {
            throw new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, MessageError.NO_SE_HA_ENCONTRADO_LA_ENTIDAD);
        } catch (DataAccessException er) {
            throw new DataNotFoundPersistenceException(LogRefServices.LOG_REF_SERVICES, MessageError.ERROR_EN_EL_ACCESO_LA_ENTIDAD, er);
        }
    }

    @Override
    public Countries getCountriesById(Integer id) {
        return Optional.ofNullable(countriesRepository.findCountriesById(id)).orElseThrow(() -> new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, "NO se encontraron países con el id " + id));
    }
}
