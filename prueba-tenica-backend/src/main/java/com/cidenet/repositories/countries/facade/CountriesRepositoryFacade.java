package com.cidenet.repositories.countries.facade;

import com.cidenet.model.entity.Countries;

import java.util.List;

/**
 * The interface Countries repository facade.
 */
public interface CountriesRepositoryFacade {
    /**
     * Gets all countries.
     *
     * @return the all countries
     */
    List<Countries> getAllCountries();

    /**
     * Gets countries by id.
     *
     * @param id the id
     * @return the countries by id
     */
    Countries getCountriesById(Integer id);
}
