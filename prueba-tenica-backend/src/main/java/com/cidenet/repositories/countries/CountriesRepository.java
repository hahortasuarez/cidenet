package com.cidenet.repositories.countries;

import com.cidenet.model.entity.Countries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * The interface Countries repository.
 */
@Repository
public interface CountriesRepository extends JpaRepository<Countries, Integer> {

    /**
     * Find countries by id countries.
     *
     * @param id the id
     * @return the countries
     */
    @Query("SELECT c FROM Countries c where c.id=:ID")
    Countries findCountriesById (@Param("ID") Integer id);
}
