package com.cidenet.repositories.employees.facade;

import com.cidenet.model.entity.Employees;

import java.util.List;

/**
 * The interface Employees repository facade.
 */
public interface EmployeesRepositoryFacade {

    /**
     * Gets all employees.
     *
     * @return the all employees
     */
    List<Employees> getAllEmployees();

    /**
     * Gets by id.
     *
     * @param id the id
     * @return the by id
     */
    Employees getById(Integer id);

    /**
     * Create employees.
     *
     * @param employees the employees
     * @return the employees
     */
    Employees create(Employees employees);

    /**
     * Exists by identification number boolean.
     *
     * @param identification the identification
     * @return the boolean
     */
    boolean existsByIdentificationNumber (String identification);

    /**
     * Exists by email boolean.
     *
     * @param email the email
     * @return the boolean
     */
    boolean existsByEmail(String email);

    /**
     * Exists by id boolean.
     *
     * @param id the id
     * @return the boolean
     */
    boolean existsById(Integer id);

    /**
     * Delete by id.
     *
     * @param id the id
     */
    void deleteById(Integer id);


}
