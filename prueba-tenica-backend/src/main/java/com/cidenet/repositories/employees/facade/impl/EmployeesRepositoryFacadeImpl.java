package com.cidenet.repositories.employees.facade.impl;

import com.cidenet.core.constants.MessageError;
import com.cidenet.core.exceptions.enums.LogRefServices;
import com.cidenet.core.exceptions.persistence.DataNotFoundPersistenceException;
import com.cidenet.model.entity.Employees;
import com.cidenet.repositories.employees.EmployeesRepository;
import com.cidenet.repositories.employees.facade.EmployeesRepositoryFacade;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The type Employees repository facade.
 */
@Component
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class EmployeesRepositoryFacadeImpl implements EmployeesRepositoryFacade {

    private final EmployeesRepository employeesRepository;

    /**
     * Instantiates a new Employees repository facade.
     *
     * @param employeesRepository the employees repository
     */
    public EmployeesRepositoryFacadeImpl(EmployeesRepository employeesRepository) {
        this.employeesRepository = employeesRepository;
    }

    @Override
    public List<Employees> getAllEmployees() {
        try {
            return Optional.of(employeesRepository.findAll())
                    .orElseThrow(() -> new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, "No se encontraron registros de empleados"));
        } catch (EmptyResultDataAccessException er) {
            throw new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, MessageError.NO_SE_HA_ENCONTRADO_LA_ENTIDAD);
        } catch (DataAccessException er) {
            throw new DataNotFoundPersistenceException(LogRefServices.LOG_REF_SERVICES, MessageError.ERROR_EN_EL_ACCESO_LA_ENTIDAD, er);
        }
    }

    @Override
    public Employees getById(Integer id) {
        return employeesRepository.findById(id).orElseThrow(() -> new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, "NO se encontraron empleados con el id " + id));
    }

    @Override
    public Employees create(Employees employees) {
        try {
            return employeesRepository.save(employees);
        } catch (IllegalArgumentException ie) {
            throw new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_CORRUPT, "Error al guardar el producto");
        } catch (DataAccessException er) {
            throw new DataNotFoundPersistenceException(LogRefServices.LOG_REF_SERVICES, MessageError.ERROR_EN_EL_ACCESO_LA_ENTIDAD, er);
        }
    }

    @Override
    public boolean existsByIdentificationNumber(String identification) {
        return employeesRepository.existsByIdentificationNumber(identification);
    }

    @Override
    public boolean existsByEmail(String email) {
        return employeesRepository.existsByEmail(email);
    }

    @Override
    public boolean existsById(Integer id) {
        return employeesRepository.existsById(id);
    }


    @Override
    public void deleteById(Integer identification) {
        employeesRepository.deleteById(identification);
    }
}
