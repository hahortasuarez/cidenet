package com.cidenet.repositories.employees;

import com.cidenet.model.entity.Employees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * The interface Employees repository.
 */
@Repository
public interface EmployeesRepository extends JpaRepository<Employees, Integer> {
    /**
     * Exists by identification number boolean.
     *
     * @param identification the identification
     * @return the boolean
     */
    boolean existsByIdentificationNumber (String identification);

    /**
     * Exists by email boolean.
     *
     * @param email the email
     * @return the boolean
     */
    boolean existsByEmail(String email);
    boolean existsById(Integer id);

    /**
     * Find employees by id employees.
     *
     * @param id the id
     * @return the employees
     */
    @Query("select e from Employees e where e.id=:ID")
    Employees findEmployeesById(@Param("ID") Integer id);
}
