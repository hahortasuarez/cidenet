package com.cidenet.repositories.areas.facade.impl;

import com.cidenet.core.constants.MessageError;
import com.cidenet.core.exceptions.enums.LogRefServices;
import com.cidenet.core.exceptions.persistence.DataNotFoundPersistenceException;
import com.cidenet.model.entity.Areas;
import com.cidenet.repositories.areas.AreasRepository;
import com.cidenet.repositories.areas.facade.AreasRepositoryFacade;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The type Areas repository facade.
 */
@Component
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class AreasRepositoryFacadeImpl implements AreasRepositoryFacade {

    private final AreasRepository areasRepository;

    /**
     * Instantiates a new Areas repository facade.
     *
     * @param areasRepository the areas repository
     */
    public AreasRepositoryFacadeImpl(AreasRepository areasRepository) {
        this.areasRepository = areasRepository;
    }

    @Override
    public List<Areas> getAllAreas() {
        try {
            return Optional.of(areasRepository.findAll())
                    .orElseThrow(() -> new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, "No se encontraron registros de areas"));
        } catch (EmptyResultDataAccessException er) {
            throw new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, MessageError.NO_SE_HA_ENCONTRADO_LA_ENTIDAD);
        } catch (DataAccessException er) {
            throw new DataNotFoundPersistenceException(LogRefServices.LOG_REF_SERVICES, MessageError.ERROR_EN_EL_ACCESO_LA_ENTIDAD, er);
        }
    }

    @Override
    public Areas getAreaById(Integer id) {
        return areasRepository.findById(id).orElseThrow(() -> new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, "NO se encontraron areas con el id " + id));
    }
}
