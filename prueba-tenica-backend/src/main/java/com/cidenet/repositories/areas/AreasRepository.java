package com.cidenet.repositories.areas;

import com.cidenet.model.entity.Areas;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface Areas repository.
 */
public interface AreasRepository extends JpaRepository<Areas, Integer> {
}
