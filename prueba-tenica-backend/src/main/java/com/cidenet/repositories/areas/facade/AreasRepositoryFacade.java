package com.cidenet.repositories.areas.facade;

import com.cidenet.model.entity.Areas;

import java.util.List;

/**
 * The interface Areas repository facade.
 */
public interface AreasRepositoryFacade {
    /**
     * Gets all areas.
     *
     * @return the all areas
     */
    List<Areas> getAllAreas();

    /**
     * Gets by id.
     *
     * @param id the id
     * @return the by id
     */
    Areas getAreaById(Integer id);
}
