package com.cidenet.repositories.typedocument;

import com.cidenet.model.entity.TypeDocuments;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * The interface Type document repository.
 */
public interface TypeDocumentRepository extends JpaRepository<TypeDocuments, Integer> {
}
