package com.cidenet.repositories.typedocument.facade.impl;

import com.cidenet.core.constants.MessageError;
import com.cidenet.core.exceptions.enums.LogRefServices;
import com.cidenet.core.exceptions.persistence.DataNotFoundPersistenceException;
import com.cidenet.model.entity.TypeDocuments;
import com.cidenet.repositories.typedocument.TypeDocumentRepository;
import com.cidenet.repositories.typedocument.facade.TypeDocumentRepositoryFacade;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * The type Type document repository facade.
 */
@Component
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class TypeDocumentRepositoryFacadeImpl implements TypeDocumentRepositoryFacade {

    private final TypeDocumentRepository typeDocumentRepository;

    /**
     * Instantiates a new Type document repository facade.
     *
     * @param typeDocumentRepository the type document repository
     */
    public TypeDocumentRepositoryFacadeImpl(TypeDocumentRepository typeDocumentRepository) {
        this.typeDocumentRepository = typeDocumentRepository;
    }

    @Override
    public List<TypeDocuments> getAllTypeDocument() {
        try {
            return Optional.of(typeDocumentRepository.findAll())
                    .orElseThrow(()-> new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, "No se encontraron registros de tipos de documentos"));
        }catch (EmptyResultDataAccessException er){
            throw new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, MessageError.NO_SE_HA_ENCONTRADO_LA_ENTIDAD);
        }catch (DataAccessException er){
            throw new DataNotFoundPersistenceException(LogRefServices.LOG_REF_SERVICES, MessageError.ERROR_EN_EL_ACCESO_LA_ENTIDAD,er);
        }
    }

    @Override
    public TypeDocuments getById(Integer id) {
        try {
            return typeDocumentRepository.findById(id).orElseThrow(()-> new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, "NO se encontraron tipos de documentos con el id " + id));
        }catch (EmptyResultDataAccessException er){
            throw new DataNotFoundPersistenceException(LogRefServices.ERROR_DATA_NOT_FOUND, MessageError.NO_SE_HA_ENCONTRADO_LA_ENTIDAD);
        }catch (DataAccessException er){
            throw new DataNotFoundPersistenceException(LogRefServices.LOG_REF_SERVICES, MessageError.ERROR_EN_EL_ACCESO_LA_ENTIDAD,er);
        }
    }
}
