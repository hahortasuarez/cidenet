package com.cidenet.repositories.typedocument.facade;

import com.cidenet.model.entity.TypeDocuments;

import java.util.List;

/**
 * The interface Type document repository facade.
 */
public interface TypeDocumentRepositoryFacade {

    /**
     * Gets all type document.
     *
     * @return the all type document
     */
    List<TypeDocuments> getAllTypeDocument();

    /**
     * Gets by id.
     *
     * @param id the id
     * @return the by id
     */
    TypeDocuments getById(Integer id);
}
