package com.cidenet.model.payload.response.base;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * The type Status dto.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StatusDTO {

    private String code;
    private String description;
}
