package com.cidenet.model.payload.response.countries;

import lombok.Builder;
import lombok.Data;

/**
 * The type Response countries.
 */
@Data
@Builder
public class ResponseCountries {
    private Integer id;
    private String country;
}
