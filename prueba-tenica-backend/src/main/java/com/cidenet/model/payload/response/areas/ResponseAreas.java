package com.cidenet.model.payload.response.areas;

import lombok.Builder;
import lombok.Data;

/**
 * The type Response areas.
 */
@Data
@Builder
public class ResponseAreas {
    private Integer id;
    private String area;
}
