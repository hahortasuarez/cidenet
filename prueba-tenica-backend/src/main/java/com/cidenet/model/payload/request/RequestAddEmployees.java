package com.cidenet.model.payload.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

/**
 * The type Request add employees.
 */
@Data
public class RequestAddEmployees {

    @Schema(example = "Horta")
    private String surname;

    @Schema(example = "Suarez")
    private String secondSurname;

    @Schema(example = "Harold")
    private String firstName;

    @Schema(example = "Andres")
    private String otherNames;

    @Schema(example = "1")
    private Integer countryId;

    @Schema(example = "1")
    private Integer typeDocumentId;

    @Schema(example = "12345")
    private String identificationNumber;

    @Schema(example = "1")
    private Integer areaId;

    private Date admissionDate;


}
