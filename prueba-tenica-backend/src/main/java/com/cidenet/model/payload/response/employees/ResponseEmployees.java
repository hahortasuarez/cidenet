package com.cidenet.model.payload.response.employees;

import com.cidenet.model.entity.Areas;
import com.cidenet.model.entity.Countries;
import com.cidenet.model.entity.TypeDocuments;
import com.cidenet.model.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The type Response employees.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResponseEmployees {

    private Integer id;

    private String surname;

    private String secondSurname;

    private String firstName;

    private String otherNames;

    private Countries countryId;

    private TypeDocuments typeDocumentsId;

    private String identificationNumber;

    private String email;

    private String admissionDate;

    private Areas area;

    private Status status;

    private String registrationDate;

    private String updateDate;

}
