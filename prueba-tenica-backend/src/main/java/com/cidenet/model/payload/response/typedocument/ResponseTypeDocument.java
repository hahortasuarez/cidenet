package com.cidenet.model.payload.response.typedocument;

import lombok.Builder;
import lombok.Data;

/**
 * The type Response type document.
 */
@Data
@Builder
public class ResponseTypeDocument {
    private Integer id;
    private String typeDocument;
}
