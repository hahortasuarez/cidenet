package com.cidenet.model.enums;

/**
 * The enum Status.
 */
public enum Status {

    /**
     * Activo status.
     */
    ACTIVO(1),
    /**
     * Inactivo status.
     */
    INACTIVO(2);

    private final Integer id;

    Status(Integer id) {
        this.id = id;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }
}
