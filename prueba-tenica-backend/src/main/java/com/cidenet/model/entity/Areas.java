package com.cidenet.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * The type Areas.
 */
@Data
@Entity
@Table(name = "areas")
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Areas implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "area")
    private String area;

    @ToString.Exclude
    @OneToMany(mappedBy = "areaId", fetch = FetchType.LAZY)
    private List<Employees> employees;

    public Areas(Integer id, String area) {
        this.id = id;
        this.area = area;
    }
}
