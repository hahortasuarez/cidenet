package com.cidenet.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

/**
 * The type Type documents.
 */
@Data
@Entity
@Table(name = "type_document")
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class TypeDocuments {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "type_document")
    private String typeDocument;

    @ToString.Exclude
    @OneToMany(mappedBy = "typeDocumentsId", fetch = FetchType.LAZY)
    private List<Employees> employees;
}
