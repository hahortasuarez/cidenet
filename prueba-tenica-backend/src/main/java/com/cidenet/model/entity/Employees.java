package com.cidenet.model.entity;

import com.cidenet.model.enums.Status;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The type Employees.
 */
@Data
@Entity
@Table(name = "employees")
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Employees implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "surname", length = 20, nullable = false)
    private String surname;

    @Column(name = "second_surname", length = 20)
    private String secondSurname;

    @Column(name = "first_name", length = 20)
    private String firstName;

    @Column(name = "other_names", length = 50)
    private String otherNames;

    @JoinColumn(name = "country_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Countries countryId;

    @JoinColumn(name = "type_document_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TypeDocuments typeDocumentsId;

    @Column(name = "identification_number")
    private String identificationNumber;

    @Column(name = "email", length = 300)
    private String email;

    @Column(name = "admission_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date admissionDate;

    @JoinColumn(name = "area_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Areas areaId;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "registration_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;

    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

}
