package com.cidenet.repositories.employees.facade;

import com.cidenet.model.entity.Employees;
import com.cidenet.repositories.employees.EmployeesRepository;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
class EmployeesRepositoryFacadeTest {

    @Mock
    private EmployeesRepository employeesRepository;

    @InjectMocks
    private EmployeesRepositoryFacade employeesRepositoryFacade;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void findAll_WhenRecordPresent_ReturnList(){
        when(employeesRepository.findAll()).thenReturn(Collections.singletonList(new Employees()));
        List<Employees> employeesList= employeesRepositoryFacade.getAllEmployees();
        assertFalse(employeesList.isEmpty());
        verify(employeesRepository,times(1)).findAll();
    }
}