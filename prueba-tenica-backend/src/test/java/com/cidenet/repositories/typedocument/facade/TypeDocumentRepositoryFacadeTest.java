package com.cidenet.repositories.typedocument.facade;

import com.cidenet.model.entity.TypeDocuments;
import com.cidenet.repositories.typedocument.TypeDocumentRepository;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;

class TypeDocumentRepositoryFacadeTest {

    @Mock
    private TypeDocumentRepositoryFacade typeDocumentRepositoryFacade;

    @InjectMocks
    private TypeDocumentRepository typeDocumentRepository;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void findAll_WhenRecordPresent_ReturnList(){
        when(typeDocumentRepository.findAll()).thenReturn(Collections.singletonList(new TypeDocuments()));
        List<TypeDocuments> employeesList= typeDocumentRepositoryFacade.getAllTypeDocument();
        assertFalse(employeesList.isEmpty());
        verify(typeDocumentRepository,times(1)).findAll();
    }

}