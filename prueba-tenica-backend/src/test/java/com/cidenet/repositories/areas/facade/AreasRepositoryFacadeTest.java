package com.cidenet.repositories.areas.facade;

import com.cidenet.model.entity.Areas;
import com.cidenet.repositories.areas.AreasRepository;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
class AreasRepositoryFacadeTest {

    @Mock
    private AreasRepository areasRepository;

    @InjectMocks
    private AreasRepositoryFacade areasRepositoryFacade;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findAll_WhenRecordPresent_ReturnList(){
        when(areasRepository.findAll()).thenReturn(Collections.singletonList(new Areas()));
        List<Areas> areasList= areasRepositoryFacade.getAllAreas();
        assertFalse(areasList.isEmpty());
        verify(areasRepository,times(1)).findAll();
    }
}