package com.cidenet.repositories.countries.facade;

import com.cidenet.model.entity.Areas;
import com.cidenet.model.entity.Countries;
import com.cidenet.repositories.countries.CountriesRepository;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
class CountriesRepositoryFacadeTest {

    @Mock
    private CountriesRepository countriesRepository;

    @InjectMocks
    private CountriesRepositoryFacade countriesRepositoryFacade;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void findAll_WhenRecordPresent_ReturnList(){
        when(countriesRepository.findAll()).thenReturn(Collections.singletonList(new Countries()));
        List<Countries> countriesList= countriesRepositoryFacade.getAllCountries();
        assertFalse(countriesList.isEmpty());
        verify(countriesRepository,times(1)).findAll();
    }

}