import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { endpoint } from '../infraestructure/endpoints/endpoint';
import { ResponseCountries } from '../models/countries/ResponseCountries';

@Injectable({
  providedIn: 'root',
})
export class CountriesService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<ResponseCountries[]> {
    return this.http.get(endpoint.countries).pipe(
      map(response => response as ResponseCountries[]),
    );
  }
}
