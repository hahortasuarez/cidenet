import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { endpoint } from '../infraestructure/endpoints/endpoint';
import { ResponseAreas } from '../models/areas/ResponseAreas';

@Injectable({
  providedIn: 'root',
})
export class AreasService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<ResponseAreas[]> {
    return this.http.get(endpoint.areas).pipe(
      map(response => response as ResponseAreas[]),
    );
  }
}
