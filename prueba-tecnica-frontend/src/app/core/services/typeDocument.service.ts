import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { endpoint } from '../infraestructure/endpoints/endpoint';
import { ResponseTypeDocument } from '../models/typeDocument/ResponseTypeDocument';

@Injectable({
  providedIn: 'root',
})
export class TypeDocumentService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<ResponseTypeDocument[]> {
    return this.http.get(endpoint.typeDocument).pipe(
      map(response => response as ResponseTypeDocument[]),
    );
  }
}
