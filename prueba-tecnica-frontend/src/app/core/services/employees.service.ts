import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { endpoint } from '../infraestructure/endpoints/endpoint';
import { ResponseEmployees } from '../models/employees/ResponseEmployees';
import { RequestEmployees } from '../models/employees/RequestEmployees';


@Injectable({
  providedIn: 'root',
})
export class EmployeesService {

  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
  constructor(private http: HttpClient) { }

  getAll(): Observable<ResponseEmployees[]> {
    return this.http.get(endpoint.employees).pipe(
      map(response => response as ResponseEmployees[]),
    );
  }

  getById(id: number): Observable<ResponseEmployees> {
    return this.http.get<ResponseEmployees>(`${endpoint.employees}/${id}`);
  }

  create(employees: RequestEmployees): Observable<RequestEmployees> {
    return this.http.post<RequestEmployees>(endpoint.employees, employees, { headers: this.httpHeaders });
  }

  update(id: string, employees: RequestEmployees): Observable<RequestEmployees> {
    return this.http.put<RequestEmployees>(`${endpoint.employees}/${id}`, employees, { headers: this.httpHeaders });
  }

  delete(identification: string): Observable<RequestEmployees> {
    return this.http.delete<RequestEmployees>(`${endpoint.employees}/${identification}`);
  }
}
