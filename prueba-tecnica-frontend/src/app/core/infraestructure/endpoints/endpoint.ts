import { environment } from '../../../../environments/environment';

export const endpoint = {

    employees: `${environment.serverUrl}/employees`,
    countries: `${environment.serverUrl}/countries`,
    areas: `${environment.serverUrl}/areas`,
    typeDocument: `${environment.serverUrl}/typeDocument`,





};
