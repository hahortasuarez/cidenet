import { ResponseAreas } from '../areas/ResponseAreas';

import { ResponseCountries } from '../countries/ResponseCountries';
import { Status } from '../enum/Status';
import { ResponseTypeDocument } from '../typeDocument/ResponseTypeDocument';

export class ResponseEmployees {

    id: number;
    surname: string;
    secondSurname: string;
    firstName: string;
    otherNames: string;
    countryId: ResponseCountries;
    typeDocumentsId: ResponseTypeDocument;
    identificationNumber: string;
    email: string;
    admissionDate: Date;
    area: ResponseAreas;
    status: Status;
    registrationDate: Date;
    updateDate: Date;

}
