import { Status } from '../enum/Status';


export class RequestEmployees {
    surname: string;
    secondSurname: string;
    firstName: string;
    otherNames: string;
    countryId: number;
    typeDocumentId: number;
    identificationNumber: string;
    areaId: number;
    admissionDate: Date;
}
