import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ResponseEmployees } from 'src/app/core/models/employees/ResponseEmployees';
import Swal from 'sweetalert2';
import { EmployeesService } from '../../core/services/employees.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {

  employees: ResponseEmployees[] = [];
  onData = true;

  constructor(private service: EmployeesService, private router: Router) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(): void {
    this.service.getAll().subscribe(
      employees => {
        this.employees = employees;
        if (this.employees.length === 0) {
          this.onData = false;
        }
      }
    );
  }

  delete(identification): void {

    Swal.fire({
      title: '¿Esta seguro de eliminar el empleado?',
      text: 'Esta operación es irreversible',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.service.delete(identification).subscribe(() => {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Empleado eliminado con exito',
            showConfirmButton: false,
            timer: 1500
          });
          this.getAll();
        });
      }
    });
  }

}
