import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { EmployeesService } from 'src/app/core/services/employees.service';
import { CountriesService } from 'src/app/core/services/countries.service';
import { AreasService } from 'src/app/core/services/areas.service';
import { TypeDocumentService } from 'src/app/core/services/typeDocument.service';

import { ResponseCountries } from 'src/app/core/models/countries/ResponseCountries';
import { ResponseAreas } from 'src/app/core/models/areas/ResponseAreas';
import { ResponseTypeDocument } from 'src/app/core/models/typeDocument/ResponseTypeDocument';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-create',
  templateUrl: './form-create.component.html',
  styleUrls: ['./form-create.component.scss']
})
export class FormCreateComponent implements OnInit {

  checkOutForm: FormGroup;
  countries: ResponseCountries[];
  areas: ResponseAreas[];
  typeDocument: ResponseTypeDocument[];

  constructor(
    private formBuilder: FormBuilder, private router: Router, private service: EmployeesService,
    private serviceCountries: CountriesService, private serviceAreas: AreasService, private typeDocumentService: TypeDocumentService) {

    this.checkOutForm = this.formBuilder.group({
      surname: ['', [Validators.required]],
      secondSurname: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      otherNames: [''],
      countryId: ['', [Validators.required]],
      typeDocumentId: ['', [Validators.required]],
      identificationNumber: ['', [Validators.required]],
      areaId: ['', [Validators.required]],
      admissionDate: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.getCountries();
    this.getAreas();
    this.getTypeDocument();
  }

  getTypeDocument(): any {
    this.typeDocumentService.getAll().subscribe(typeDocument => {
      this.typeDocument = typeDocument;
    });
  }

  getCountries(): any {
    this.serviceCountries.getAll().subscribe(countries => {
      this.countries = countries;
    });
  }

  getAreas(): any {
    this.serviceAreas.getAll().subscribe(areas => {
      this.areas = areas;
    });
  }

  create(employee): any {
    console.log(employee);
    this.service.create(employee).subscribe(() => {
      this.router.navigate(['/employees']);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Empleado guardado con exito',
        showConfirmButton: false,
        timer: 1500
      });
    },
      (err) => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: err.error.detailMessage,
        });
      });
  }

  cancelar(): any {
    this.router.navigate(['/employees']);
  }

  mayus(e): any {
    console.log(e);
    e.value = e.value.toUpperCase();
  }


}
