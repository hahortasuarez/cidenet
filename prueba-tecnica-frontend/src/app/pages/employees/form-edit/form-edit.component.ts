import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseAreas } from 'src/app/core/models/areas/ResponseAreas';
import { ResponseCountries } from 'src/app/core/models/countries/ResponseCountries';
import { ResponseEmployees } from 'src/app/core/models/employees/ResponseEmployees';
import { ResponseTypeDocument } from 'src/app/core/models/typeDocument/ResponseTypeDocument';
import { AreasService } from 'src/app/core/services/areas.service';
import { CountriesService } from 'src/app/core/services/countries.service';
import { EmployeesService } from 'src/app/core/services/employees.service';
import { TypeDocumentService } from 'src/app/core/services/typeDocument.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-form-edit',
  templateUrl: './form-edit.component.html',
  styleUrls: ['./form-edit.component.scss']
})
export class FormEditComponent implements OnInit {

  employees: ResponseEmployees;
  checkOutForm: FormGroup;
  countries: ResponseCountries[];
  areas: ResponseAreas[];
  typeDocument: ResponseTypeDocument[];
  admissionDate: Date;

  constructor(
    private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private service: EmployeesService, private router: Router,
    private serviceCountries: CountriesService, private serviceAreas: AreasService, private typeDocumentService: TypeDocumentService) {

    this.checkOutForm = this.formBuilder.group({
      surname: ['', [Validators.required]],
      secondSurname: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      otherNames: [''],
      countryId: [''],
      typeDocumentId: [''],
      identificationNumber: ['', [Validators.required]],
      areaId: [''],
      admissionDate: ['', [Validators.required]],
    });

  }

  ngOnInit(): void {
    console.log(this.admissionDate);
    this.getEmployeeById();
    this.getCountries();
    this.getAreas();
    this.getTypeDocument();
  }

  getEmployeeById(): void {
    this.activatedRoute.params.subscribe(params => {
      const id = params.id;
      if (id) {
        this.service.getById(id).subscribe(employees => {
          this.employees = employees;
          if (!(employees.admissionDate instanceof Date)) {
            this.admissionDate = new Date(employees.admissionDate);
            console.log(this.admissionDate);
          }
        });
      }
    });
  }


  getTypeDocument(): any {
    this.typeDocumentService.getAll().subscribe(typeDocument => {
      this.typeDocument = typeDocument;
    });
  }

  getCountries(): any {
    this.serviceCountries.getAll().subscribe(countries => {
      this.countries = countries;
    });
  }

  getAreas(): any {
    this.serviceAreas.getAll().subscribe(areas => {
      this.areas = areas;
    });
  }

  update(id, employee): any {
    if (employee.countryId === '') {
      employee.countryId = this.employees.countryId.id;
    }
    if (employee.typeDocumentId === '') {
      employee.typeDocumentId = this.employees.typeDocumentsId.id;
    }
    if (employee.areaId === '') {
      employee.areaId = this.employees.area.id;
    }
    if (!(employee.admissionDate instanceof Date)) {
      employee.admissionDate = new Date(employee.admissionDate);
    }

    this.service.update(id, employee).subscribe(() => {
      this.router.navigate(['/employees']);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Empleado guardado con exito',
        showConfirmButton: false,
        timer: 1500
      });
    },
      (err) => {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: err.error.detailMessage,
        });
      });
  }

  cancelar(): any {
    this.router.navigate(['/employees']);
  }

}
