import { Routes, RouterModule } from '@angular/router';
import { EmployeesComponent } from './pages/employees/employees.component';
import { FormCreateComponent } from './pages/employees/form-create/form-create.component';
import { FormEditComponent } from './pages/employees/form-edit/form-edit.component';

const routes: Routes = [
  { path: 'employees', component: EmployeesComponent },
  { path: 'employees/form', component: FormCreateComponent },
  { path: 'employees/form/:id', component: FormEditComponent },
  {path: '**', pathMatch: 'full', redirectTo: 'employees'}

];

export const AppRoutingModule = RouterModule.forRoot(routes);

